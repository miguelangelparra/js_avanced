//ESTRUCTURA DE DATOS - OBJETOS 


let imp = function (e) {
    console.log(e);
}

let obj = {};
obj.nombre = "Maria";
//obj.serNombre(valor)   asi seria en lenguajes tradicionales
obj['apellido'] = 'Rodriguez';
/*
    {
        nombre:"Maria",
        apellido: "Rodriguez",
        saludar: function(){},
        mascota: {

        }
    }

    En Js Los objetos siempre heredan de otro objeto llamado prototipo
*/
console.log(obj);

let objDue = {
    nombre: "Ramon",
    apellido: "Perez",
    nombreCompleto: function () {
        console.log(`Mi nombre es ${this.nombre} y mi apellido es ${this.apellido}`)
    }
}
objDue.nombreCompleto();


var persona = {
    nombre: "Jose",
    apellido: "Perez",
}

persona.edad = 33;
persona.esMayor = function () {
    if (this.edad > 18) {
        console.log("Es mayor de edad");
    } else {
        console.log("No es mayor")
    }
}

persona.edad = 12;
persona.esMayor()
persona.edad = 45;
persona.esMayor()

//delete persona.edad;
console.log(persona)


let otroObj = new Object();
console.log(typeof otroObj);
imp("El contenido del objeto recien creado es: " + otroObj)
imp("El contenido del objeto recien creado es: " + JSON.stringify(otroObj))

/*
CONSTRUCTORES
Es una funcion que se usa para crear nuevos onjetos. 
Los objetos pueden ser creados usando el constructor mediante estos dos pasos:
1) Definir el objeto original definienl el constructor.
Por convencion el nombre del constructor debe comenzar con mayuscula. 
2) Debemos instanciar el objeto mediante la plabra reservada "new"
*/
let body = document.querySelector('body')
function agregarP(data) {
    let p = document.createElement('p');
    p.innerHTML = data;
    let body = document.querySelector('body');
    body.appendChild(p);

}
/* en  java seria:

class Persona{
    private String nombre;
    private String apellido;
    private Persona (String n, String a)
    this.nombre= n;
    this.apellido = a;

        
        */


function OtraPersona(nombre, apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
}

function OtraPersona2(n, a) {
    this.nombre = n;
    this.apellido = a;
}

let personaUno = new OtraPersona('Maria', 'Rodriguez');
let titulo = document.createElement('h1');
titulo.innerHTML = ' Objeto Persona';
body.appendChild(titulo);
agregarP("Tipo de dato Persona: " + typeof personaUno);
agregarP("Nombre de la persona: " + personaUno.nombre);
agregarP("Apellido de la persona: " + personaUno.apellido);

function crearTitulo(tituloTexto) {
    let titulo = document.createElement('h1');
    titulo.innerHTML = tituloTexto;
    body.appendChild(titulo);

}




crearTitulo('Funciones Variadicas')

/* En js no se puede sobrecargar un metodo. 
function saludar(nombre){
    console.log("hola soy " + nombre ); 
} 
function saludas(nombre,edad){
    console.log("tengo x años")
}

interface -> contrato
class Persona implemets Humano
Persona p = new Persona();
Humano p1 = new Persona()
   */

function v1() {
    agregarP("Primer argumento: " + arguments[0])

    agregarP("Segundo Argumento: " + arguments[1])

    agregarP("Tercer Argumento: " + arguments[2])

    agregarP("Cuarto Argumento: " + arguments[4])
}

v1(1, 2, 3)


function datos() {
    if (typeof arguments[1] !== 'undefined') {
        if (isNaN(arguments[1])) {
            agregarP('Me pasaste como parametro un String')
        } else {
            agregarP('Me pasaste como parametro la edad')
        }
    } else {
        agregarP('No me pasaste nada')
    }
}

datos('Pedro')
datos('Pedro', 'Lola')
datos('Pedron', 333)


function otroDato(...params) {
    agregarP(params);
    console.info(params)
}
// "..." Es la destructuracion = te convierte en array lo que le pases a la funcion     

otroDato('Pedro')
otroDato('Pedro', 'Lola')
otroDato('Pedron', 333)





//Object Create
crearTitulo('Object Create');

let objetoNuevo = Object.create(null);
objetoNuevo.campo = 'Soy un campo';
agregarP(objetoNuevo.campo)


//Recibe un object o un null + otro parametro que son las propiedades iniciales
/** Object create descriptors
 * 1-configurable: V => pueden ser alteradas las propiedades
 * 2-Enumerable: V => Se puede ver las propiedades del objeto
 * 3-value: Valor asociado a la propiedad
 * 4-writable.: V => que lo puedo modificar el value
 */
let objetoOtro = Object.create(null);
Object.defineProperty(objetoOtro, 'prop', {
    value: ' Hola que tal? ',
    writable: true,
    enumerable: true,
    configurable: true
})
console.log(objetoOtro)


//Getters y setters

//Un Ejemplo
/*
Object.defineProperty(objetoOtro, 'otro', {
    get(){return valor},
    //set(nuevoValor){valor= nuevoValor},    seteable
    set(){valor= 12356775555}, //no seteable
    enumerable: true,
    configurable: true
})*/

//Otro Ejemplo
Object.defineProperty(objetoOtro, 'otro', {
    get(){return 'Holis'}, //Si solo get entonces no es seteable 
    enumerable: true,
    configurable: true
})
objetoOtro.otro = 123
console.log(objetoOtro)
