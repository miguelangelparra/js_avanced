
/*HERENCIA CON ECMA 6 */
class Persona{
    constructor(nombre,documento){
        this.nombre=nombre;
        this.documento=documento;
    }

    comer(){
        return `${this.nombre} esta comiendo`;
    }

    dormir(){
        return `${this.nombre} esta durmiendo`;
    }

    viajar(){
        return `${this.nombre} esta viajando`
    }
}


class Alumno extends Persona{
    constructor(nombre,documento,clase){
        super(nombre,documento);
        this.clase=clase;
    }
    estudiar(){
        return `El alumno  ${this.nombre} esta estudiando!`
    }

    irAClase(){
        return ` El alumno ${this.nombre} esta en clase de ${this.clase}`
    }
}

function display(param){
    console.log(param)
}

let a1 = new Alumno('Maria',4555564,'Js')
let a2 = new Alumno('Pedro',98774854,'Java');

display(a1.viajar())
display(a2.irAClase())

p1=  new Persona ('Andrea',7889956);
//p1.irAClase() // Va a lanzar error por que la clase persona no tiene este metodo.





/*HERENCIA CON ECMA 5  */


/*cuando se quieren hacer metodos heredables se deben setear en los prototipos*/
function PersonaProt(nombre,documento){
    this.nombre = nombre;
    this.documento = documento;
}

PersonaProt.prototype.comer= function(){
    return `${this.nombre} esta comiendo`;
}

PersonaProt.prototype.dormir = function(){
    return `${this.nombre} esta durmiendo` 
}

console.dir(PersonaProt);
display(PersonaProt)

function AlumnoProt(nombre,documento,clase) {
    PersonaProt.call(this,nombre,documento); //Esto va a reemplazar al super
    //Es como llamar al constructor padre, es un super. 0
    this.clase = clase;
}

AlumnoProt.prototype=Object.create(PersonaProt.prototype); //Esto reemplazaria el extends
AlumnoProt.prototype.constructor = AlumnoProt;

AlumnoProt.prototype.estudiar=function (){
    return `el alumno ${this.nombre} esta estudiando`;
}

AlumnoProt.prototype.irAClase = function (){
    return `el alumno ${this.nombre} esta en la clase de ${this.clase}`;
}

let a3 =  new AlumnoProt('pablo',134123123,'Css');
display(a3.estudiar());
display(a3.irAClase());



console.dir(AlumnoProt)
/* function AlumnoProt(nombre,documento,clase) {
    PersonaProt.call(this,nombre,documento)
// esto es igual a 
this.nombre =nombre
this.documento = documento
this.clase= clase*/





console.log(window)
console.log(window.href)
console.log(navigator)
console.log(navigator.geolocation)
console.log(navigator.geolocation.getCurrentPosition)
console.log(navigator.geolocation.watchPosition)





