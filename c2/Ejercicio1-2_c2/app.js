//let input2 = document.querySelector("#e2");
let input1 = document.querySelector("#e1");
let input2 = document.getElementById('e2');   //Es igual que el de arriba

//Formas de agregar al listener: 
//Forma 1
//input2.onblur= function(){console.log("soy el evento onblur")}

//Forma 2: 
/*input2.addEventListener('blur',() => {             
  //  console.log("soy el evento onblur")                            
    if(input1.value == input2.value){                   //Esto esta validando el email
        input2.classList.add("ok");
        input2.classList.remove("error");
        } else{
            input2.classList.add("error");
            input2.classList.remove("ok");
        }
})*/

//forma3:  ejecutando el evento directamnte en el elmento del dom, en el archivo html.



//Otra forma de validar el email: atraves de una funcion aparte que se puede enviar como callbak a un evento: 

function comparar () {
    if(input1.value == input2.value){
        input2.classList.add("ok");
        input2.classList.remove("error");
        } else{
            input2.classList.add("error");
            input2.classList.remove("ok");
        }
}


input2.addEventListener('blur',comparar)               // Esto es una forma de ejecutar por callback

