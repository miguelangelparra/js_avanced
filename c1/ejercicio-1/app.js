//Funcion imprimir 
function imp(x) {
    console.log(x);
}
//Var permite redefinir y modificar el dato
var a = 14;
var a = "Hola"
a = "hola";
//Let permite modificar el dato pero no permite redefinirlo 
let b = true;
//let b = false;
//Const Es una constante, no se modifica nunca
const c = "Esto No cambia";

//Funcion con IF comprueba sin dos valores son iguales segun dato y tipo de dato 
let j = 3;
let i = "3";
//Segun dato
if (i == j) {
    imp("Son iguales");
} else {
    imp("No son iguales");
}
//Segun tipo de dato 
if (i === j) {
    imp("Son iguales");
} else {
    imp("No son iguales");
}

let size = 8;
let arr = ['hola', 'como', 'estas?'];

//Bucles y Repeticiones
//For(variable FLAG; condicion para parar ; Accion despues de cada iteracion )
for (let i = 0; i < arr.length; i++) {
    imp(arr[i] + " ")
}

for (let line = '#'; line.length < 8; line += '#') {
    imp(line)
}