let progress = document.querySelector("progress")
let youtube = document.querySelector("#youtube-loader")

let xhr = new XMLHttpRequest
xhr.open("GET","imagen.jpg")

xhr.addEventListener("readystatechange",()=>{
    if (xhr.readyState == 2) {
        progress.classList.toggle("hidden")
    }
})

xhr.addEventListener("progress",e=>{
    if (e.lengthComputable) {
        let porcentaje = e.loaded / e.total * 100
        progress.value = porcentaje   
        youtube.style.width = porcentaje+"%"
    }else{
        //No puedo calcular cuanto va a demorar la descarga
    }
})
xhr.addEventListener("load",()=>{
    console.log("Termino la descarga")
    progress.classList.toggle("hidden")
    youtube.style.width = ""
})
xhr.send()

/**
 * Elemento.classList.add("") : Agrega una clase
 * Elemento.classList.remove("") : remueve una clase
 * Elemento.classList.toggle("") : Activa o desactiva una clase dependiendo su estado actual
 * 
 */