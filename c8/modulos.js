let moduloApp = (function () {
    var contador = 0;


    //Todo lo que esta dentro del return es la parte publica, lo que esta detras del return es privado. 
    return {
        incrementarContador: function () {
            return contador++;
        },

        resetContador: function () {
            console.log(`El valor del contador antes de resetear es : ${contador}`);
            contador = 0;
        }
    }
})();

//Esto no es publico por eso por consola sale undefined
console.log(moduloApp.contador)


/*
let m = moduloApp();
m.incrementarContador();
m.resertContador();
console.log(moduloApp());
*/

moduloApp.incrementarContador();
moduloApp.incrementarContador();
moduloApp.incrementarContador();
moduloApp.incrementarContador();
moduloApp.incrementarContador();
moduloApp.incrementarContador();
moduloApp.resetContador();

let carritoModulo = (function () {
    let carrito = [];
    const IVA = 1.21;

    function calcularIVA(precio) {
        return precio * IVA
    }

    return {
        addItem: function (item) {
            carrito.push(item);
        },

        getItemsTotal: function () {
            return carrito.length;
        },

        getTotal: function () {
            let q = this.getItemsTotal();
            p = 0;

            while (q--) {
                p += calcularIVA(carrito[q].precio);
            }
        }
    }
})();

carritoModulo.addItem({
    item: 'Pan',
    precio: 100
});
console.log(carritoModulo.getItemsTotal());
console.log(carritoModulo.getTotal());