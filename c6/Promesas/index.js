// El constructor de una promesa
let p = new Promise(function (resolve, reject) {
    //   setTimeout(()=>resolve("Listo"),1000)
    setTimeout(() => reject(new Error("Se rompio"), 1000))
})

p.then(
    function (result) { console.log(result) },
    function (error) { console.log(error) }
)

//Incialmente una promesa ==> Pending -> fullfiled  /  reject
// result y error son parametros predeterminados como respuesta de la promesa 



//Encadenamiento de Promesas: 
let promesa = new Promise(function (resolve, reject) {
    setTimeout(() => resolve(1), 1000)
})

promesa.then(function (result) {
    console.log(result);
    return result * 2;
})
promesa.then(function (result) {
    console.log(result);
    return result * 2;
})

new Promise(function (resolve, reject) {
    setTimeout(() => resolve(1), 1000)
}).then(function (result) {
    console.log(result)
    return result * 2
}).then(function (result) {
    console.log(result)
    return result * 2
}).then(function (result) {
    console.log(result)
    return result * 2
})