

//Forma completa
/*fetch("http://jsonplaceholder.typicode.com/posts")
.then((response) => {
    return response.json();
}).then((myJson)=>{
    console.log(JSON.stringify(myJson));
})*/

//Forma simplificada con arrow function 
fetch("http://jsonplaceholder.typicode.com/posts")
.then((response) => response.json()).then((myJson)=>console.log(JSON.stringify(myJson)));

