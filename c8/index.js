let obj = {};
obj.prop="";
obj['otroProp']='';

function Persona(nombre,apellido){
    this.nombre=nombre;
    this.apellido=apellido;
}

let p = new Persona('Maria', 'Perez');

function Animal(edad){
    this.edad=edad
}

Animal.prototype.crecer= function(){
    this.edad++;
    return this.edad;
}

let a = new Animal(4);
console.log(a.edad);
a.crecer();
console.log(a.edad);
console.log(a);

function Gato(){}

Gato.prototype = new Animal();
Gato.prototype.constructor = Gato;
console.log(Gato)

Gato.prototype.maullar= function(){
    console.log("Miauuu")
}

let g = new Gato()
console.log(g)




function Empleado(nombre,dni,sueldo){
    this.nombre=nombre;
    this.dni=dni;
    this.sueldo=sueldo;
}

Empleado.prototype.liquidacion = function(){
    console.log("El sueldo del empleado es: " + this.sueldo);
}

function EmpleadoVenta(sueldo){
    this.sueldo=sueldo;
}

function constructorEmpleadoVenta(){}

EmpleadoVenta.prototype=new Empleado("Jose",5597446,25000);
EmpleadoVenta.prototype.constructor = constructorEmpleadoVenta;

let e = new Empleado('Jose','45566855', 20000);
console.log(e);

let ev = new EmpleadoVenta()
console.log(ev);
console.log(ev.__proto__);


console.log("---------------------------------------------------------------------")

function Pajaro(tipo,color){
    this.tipo = tipo;
    this.color = color;

    this.volar = function(){
        console.log(`${this,color} ${this.tipo} esta volando`);
    }

    this.caminar = function(){
        console.log(`${this,color} ${this.tipo} esta caminando`);
    }
}

function Loro (tipo,color,poder){
    Pajaro.call(this,tipo,color)
    this.poder=poder
    this.hablar = function () {
console.log(` Soy un ${this,color} ${this.tipo} y estoy Hablando, Truaaa! mi poder es  ${this.poder}`)
    }
}

let l = new Loro ('Loro', 'Verde', 'desaparecer');
l.hablar();


let objeto= {
    nombre : "Bruce Wayne",
    me : function(){
        this.nombre= "Batman";
        console.log(this);
    }
}
console.log(objeto.nombre)
objeto.me();

const PersonaOtra = {
    name:'Bruce',
    lastName: 'Wayne',
    fullName: function(){
        return `${this.name} ${this.lastName}`;
    }
}

const print = function(saludo, adj){
    console.log(saludo, this.fullName(), 'vos sos ', adj)
};

//Esto ejecuta la funcion como si estuviera dentro de persona
const printBind = print.bind(PersonaOtra);
printBind('Hola', 'Gordo')



let auto = {
    patente: "AaC4558",
    marca: "Toyota",

    mostrarDetalles: function(){
        console.log(`${this.patente} - ${this.marca}`)
    }
}
let otroAuto = {
    patente: "gfsd666",
    marca: "Chevrolet",
}

auto.mostrarDetalles();

let detalleAuto = auto.mostrarDetalles;
detalleAuto();

impDetalleAuto= detalleAuto.bind(otroAuto);
impDetalleAuto();




function as(prop){
    this.me= this;
    this.param= prop;
    this.func= function(){
        param=232223;
        console.log(param,this.me.param);
    }
}
let asd= new as(324);
asd.func();