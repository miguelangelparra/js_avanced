let habitaciones = document.querySelectorAll(".habitacion");

function esconder () {
    for(let i = 0;  i < habitaciones.length; i++){
        habitaciones[i].classList.add('esconder'); 
    }
}


let selectHabitaciones = document.querySelector('select[name=habitacion]');

selectHabitaciones.addEventListener('change', mostrarOpcion);

function mostrarOpcion() {
    let valor = selectHabitaciones.value;
    esconder();
    if(valor==0){
        return false;
    }
    document.querySelector('.habitacion--'+valor).classList.remove('esconder');
}