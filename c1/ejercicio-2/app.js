let titulo = document.querySelector('h2');
titulo.innerHTML = "Otro titulo"

//1) Crear un elemento
let txt_num1 = document.createElement('input');
let txt_num2 = document.createElement('input');
//2) Setear atributos al elemento creado
txt_num1.value = 0
txt_num2.value = 0
txt_num1.setAttribute('type', 'number');
txt_num2.setAttribute('type', 'number');
txt_num1.setAttribute('id', 'num1');
txt_num2.setAttribute('id', 'num2');
// Agregar clases
txt_num1.classList.add('form-control');
txt_num2.classList.add('form-control');
//3) Buscar el elemento que ya existe en el DOM 
let div1 = document.querySelector('#n1');
//4) Agregar el nuevo elemento creado al elemento existente en el dom 
div1.appendChild(txt_num1);

let div2 = document.querySelector('#n2');
div2.appendChild(txt_num2);




function ejecutar() {
    console.log("Hiciste Click")
        //capturamos el valor de la opcion que està seleccionada en el select
    let seleccion = document.getElementById("op");
    let operacion = seleccion.options[seleccion.selectedIndex].value;
    console.log(operacion)
        //Capturamos los valores de los inputs
    let num1 = Number(document.getElementById("num1").value);
    //Muestra por consola el tipo de dato 
    console.log(typeof num1)
    let num2 = Number(document.getElementById("num2").value);

    //Creamos la variable resultado 
    let resultado = 0
        //Validacion de que los numeros no sean vacios
    if (num1 == 0 || num2 == 0) {
        return false;
    }
    // Una forma de hacerlo utilizando condiciones if
    /*
        if (operacion === "1") {
            resultado = num1 + num2;
        } else if (operacion === "2") {
            resultado = num1 - num2;
        } else if (operacion === "3") {
            resultado = num1 * num2;
        } else if (operacion === "4") {
            resultado = num1 / num2;
        }*/
    // Otra forma de hacerlo utilizando un switch-case
    switch (operacion) {
        case "1":
            resultado = num1 + num2;
            break;
        case "2":
            resultado = num1 - num2;
            break;
        case "3":
            //Validamos con un if que no sea posible dividir entre 0 
            if (num2 == 0) {
                return false
            }
            resultado = num1 * num2;
            break;
        case "4":
            resultado = num1 / num2;
            break;
        default:
            resultado = "No existe la operacion seleccionada";
            break
    }
    //Muestra resultado en el label del html 
    document.querySelector("#resultado").innerHTML = resultado
}