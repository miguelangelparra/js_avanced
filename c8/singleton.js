//Patron Singleton
//Se suele utilizar para base de datos (conexiones)
// Esto le pega a la misma instancia sin importar el numero de objetos


let miSingleton = (function () {
    let instancia;

    function init() {
        console.log('instancia')

        let numeroAleatorio = Math.random();

        return {
            getNumeroAleatorio: function () {
                return numeroAleatorio;
            }
        }
    }
    return {
        getInstancia: function () {
            if (!instancia) {
                instancia = init();
            }
            return instancia;
        },
    }

})();

let singleA = miSingleton.getInstancia()
let singleB = miSingleton.getInstancia()
let singleC = miSingleton.getInstancia()

console.log(singleA.getNumeroAleatorio())
console.log(singleB.getNumeroAleatorio())
console.log(singleC.getNumeroAleatorio())