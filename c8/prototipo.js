//Patron de Prototipo

let vehiculoPrototipo = {
    init: function(modeloAuto){
        this.modeloAuto=modeloAuto;
    },
    getModelo: function(){
        console.log(`Èl model de este vehiculo es ${this.modeloAuto}`);
    }
}

function vehiculo(modeloAuto){
    function F(){}
        F.prototype = vehiculoPrototipo;
        let f = new F();

        f.init(modeloAuto);
        return f;

    }


let auto= vehiculo('Ford Focus');
auto.getModelo();